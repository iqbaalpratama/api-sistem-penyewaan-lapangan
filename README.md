# API Sistem Penyewaan Lapangan

REST API untuk sistem penyewaan lapangan

Link deploy heroku: https://booking-system-main-bersama.herokuapp.com/

Keterangan tambahan:
- Untuk dokumentasi ada pada link berikut: https://booking-system-main-bersama.herokuapp.com/docs/index.html
- Untuk bisa mengakses aplikasi sebagai owner, dapat mendaftar akun sebagai owner dengan mengisikan di form request body saat register yaitu field role dengan value owner.

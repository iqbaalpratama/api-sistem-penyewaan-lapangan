import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Verify {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let verified = auth.user?.is_verified
    if(verified)
    {
          await next()
    }
    else
    {
        response.unauthorized({message: 'Your account is not verified, please confirm your OTP Code'})
    }
  }
}

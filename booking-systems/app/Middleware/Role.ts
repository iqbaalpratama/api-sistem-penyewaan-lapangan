import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Role {
  public async handle ({ auth, response}: HttpContextContract, next: () => Promise<void>, allowedRoles: string[]) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let check = 0
    allowedRoles.map( role => {
        if(role == auth.user?.role)
        {
          check = 1
        }
    })
    if(check)
    {
      await next()
    }
    else
    {
      return response.unauthorized({ message: 'Your role cannot access this routes, check again your role'})
    }

  }
}

import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
  ManyToMany,
  manyToMany
} from '@ioc:Adonis/Lucid/Orm'
import Booking from 'App/Models/Booking'
import Venue from 'App/Models/Venue'

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public role: string

  @column()
  public is_verified: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }
  @hasMany( () => Venue,{
    foreignKey: 'user_id',
  })
  public venues: HasMany<typeof Venue>

  @hasMany( () => Booking,{
    foreignKey: 'user_id',
  })
  public bookings: HasMany<typeof Booking>

  @manyToMany(() => Booking, {
    pivotTable: 'users_bookings',
    localKey: 'id',
    pivotForeignKey: 'user_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'booking_id'
  })
  public players: ManyToMany<typeof Booking>
}

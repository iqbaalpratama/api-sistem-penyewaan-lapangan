import { DateTime } from 'luxon'
import {
  BaseModel,
  column,
  belongsTo,
  BelongsTo,
  manyToMany,
  ManyToMany
} from '@ioc:Adonis/Lucid/Orm'
import Field from 'App/Models/Field'
import User from 'App/Models/User'

export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime()
  public play_date_start: DateTime

  @column.dateTime()
  public play_date_end: DateTime

  @column()
  public user_id: number

  @column()
  public field_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Field, {
    foreignKey: 'field_id',
    localKey: 'id'
  })
  public field: BelongsTo<typeof Field>

  @belongsTo(() => User,{
    foreignKey: 'user_id',
    localKey: 'id'
  })
  public user: BelongsTo<typeof User>

  @manyToMany(() => User, {
    pivotTable: 'users_bookings',
    localKey: 'id',
    pivotForeignKey: 'booking_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'user_id'
  })
  public players: ManyToMany<typeof User>
}

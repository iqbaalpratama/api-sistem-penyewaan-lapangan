import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueValidator from 'App/Validators/VenueValidator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'
export default class VenuesController {
/**
  * @swagger
  * /api/v1/venues:
  *   get:
  *     tags:
  *         - Venues
  *     security:
  *         - bearerAuth: []
  *     summary: Get all data venue
  *     responses:
  *       200:
  *         description: success get all data venue
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Not found
  *       400:
  *         description: Failed get all data venue
  *   post:
  *     tags:
  *         - Venues
  *     security:
  *         - bearerAuth: []
  *     summary: Store data venue
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          name:
  *                              type: string
  *                          address:
  *                              type: string
  *                          phone:
  *                              type: string
  *                      required:
  *                          - name
  *                          - address
  *                          - phone
  *     responses:
  *       201:
  *         description: success store data field
  *       401:
  *         description: User Unauthorized
  *       400:
  *         description: Failed store data venue, fails validate data request
  * /api/v1/venues/{id}:
  *   get:
  *     tags:
  *         - Venues
  *     security:
  *         - bearerAuth: []
  *     summary: Get data venue by id
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: venue id to get
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: success show data venue by id
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Not found
  *       400:
  *         description: Failed show data venue
  *   put:
  *     tags:
  *         - Venues
  *     security:
  *         - bearerAuth: []
  *     summary: Update data venue
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: venue id to update
  *        schema:
  *          type : integer
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          name:
  *                              type: string
  *                          address:
  *                              type: string
  *                          phone:
  *                              type: string
  *                      required:
  *                          - name
  *                          - address
  *                          - phone
  *     responses:
  *       201:
  *         description: success update data venue
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Venue not found
  *       400:
  *         description: Failed update data venue, fails validate data request
  */
    public async index({response}: HttpContextContract) {
        try
        {
            const venue = await Venue
            .query()
            .select('venues.id','venues.name','venues.address', 'venues.phone')
            .preload('fields',(fieldsQuery) => {
                fieldsQuery.select('fields.id','fields.name', 'fields.type')
            })
            return response.ok({ message: 'Success!', data: venue})
        } catch (error)
        {
            return response.badRequest({message: 'Failed to get data venue', error: error.messages})
        }
    }
    public async store({request, response, auth}: HttpContextContract) {

        try {
            const data = await request.validate(VenueValidator)
            const venue = new Venue()
            venue.name = data.name
            venue.address = data.address
            venue.phone = data.phone
            const user = auth.user
            await user?.related('venues').save(venue)
            return response.created({ message: 'Created new venue!'})
        } catch (error) {
            if(error.messages != undefined)
            {
                return response.badRequest({message: 'Failed to create new venue', error: error.messages})
            }
            else
            {
                return response.unprocessableEntity({message: 'Failed to create new venue', error: error.message})
            }

        }
    }
    public async show({request, response}: HttpContextContract) {
        try {
            const venue = await Venue
            .query()
            .select('venues.id','venues.name','venues.address', 'venues.phone')
            .preload('fields',(fieldsQuery) => {
                fieldsQuery.select('fields.id','fields.name', 'fields.type')
            })
            .where('id', request.params().id)
            .first()
            if(venue == null)
            {
                return response.notFound({message: 'Failed show venue', error: 'Venue not found'})
            }
            else
            {
                return response.ok({ message: 'Success!', data: venue})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed show venue', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed show venue', error: error.message})
            }
        }
    }

    public async update({request, response, auth}: HttpContextContract) {
        try {
            await request.validate(VenueValidator)
            const user = await auth.user
            const data = await Venue
            .query()
            .preload('user')
            .where('fields.id', parseInt(request.params().id))
            .first()
            if(data?.user.id != user?.id)
            {
                return response.unauthorized({ message: 'You dont have right to update this venue'})
            }
            else
            {
                const venue = await Venue.findOrFail(request.params().id)
                venue.name = request.input('name'),
                venue.address = request.input('address'),
                venue.phone = request.input('phone')
                await venue.save()
                return response.ok({ message: 'Success edited venue!'})
            }
        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed edited venue', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed edited venue', error: error.message})
            }

        }
    }
}

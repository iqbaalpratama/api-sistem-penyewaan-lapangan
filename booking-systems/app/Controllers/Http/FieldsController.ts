import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import FieldValidator from 'App/Validators/FieldValidator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
/**
  * @swagger
  * /api/v1/venues/{id}/fields:
  *   get:
  *     tags:
  *         - Fields
  *     security:
  *         - bearerAuth: []
  *     summary: Get all field data based on id venue
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: venue id to get the field
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: success get all field data based on id venue
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Field not found
  *       400:
  *         description: Failed get all data field
  *   post:
  *     tags:
  *         - Fields
  *     security:
  *         - bearerAuth: []
  *     summary: Store data field based on id venue
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: venue id to store the field
  *        schema:
  *          type : integer
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          name:
  *                              type: string
  *                          type:
  *                              type: string
  *                              enum:
  *                                 - soccer
  *                                 - minisoccer
  *                                 - futsal
  *                                 - basketball
  *                                 - volleyball
  *                      required:
  *                          - name
  *                          - type
  *     responses:
  *       201:
  *         description: Success store data field
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Field not found
  *       400:
  *         description: Failed store data field, fails validate data request
  * /api/v1/venues/{id}/fields/{field_id}:
  *   get:
  *     tags:
  *         - Fields
  *     security:
  *         - bearerAuth: []
  *     summary: Get data field by field id and based on id venue
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: venue id to get the field
  *        schema:
  *          type : integer
  *      - name: field_id
  *        in: path
  *        required: true
  *        description: field id to get
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: success show data field by field id and based on id venue
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Field or Venue Not found
  *       400:
  *         description: Failed show data field
  */
    public async index({request, response}: HttpContextContract) {
        try {
            const field = await Field
            .query()
            .preload('venue', (venueQuery) => {
                venueQuery.select('name', 'address', 'phone')
            })
            .preload('bookings', (bookingsQuery) => {
                bookingsQuery.select('bookings.id','bookings.field_id', 'bookings.play_date_start', 'bookings.play_date_end', 'bookings.user_id')
                bookingsQuery.preload('players', (playersQuery) => {
                    playersQuery.select('name', 'email')
                })
            })
            .where('venue_id', request.params().id)
            if( field != null)
            {
                return response.ok({ message: 'Success!', data: field})
            }
            else
            {
                return response.notFound({ message: 'Failed get field data'})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed get field data', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed get field data', error: error.message})
            }
        }
    }
    public async store({request, response}: HttpContextContract) {
        try {
            const data = await request.validate(FieldValidator)
            const venues = await Venue.findOrFail(parseInt(request.params().id))
            await venues.related('fields').create({
                name: data.name,
                type: data.type
            })
            return response.created({ message: 'Success create new field'})
        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed create new field', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed create new field', error: error.message})
            }
        }
    }
    public async show({request, response}: HttpContextContract) {
        try {
            const field = await Field
            .query()
            .preload('venue', (venueQuery) => {
                venueQuery.select('name', 'address', 'phone')
            })
            .preload('bookings', (bookingsQuery) => {
                bookingsQuery.select('bookings.id','bookings.field_id', 'bookings.play_date_start', 'bookings.play_date_end', 'bookings.user_id')
                bookingsQuery.preload('players', (playersQuery) => {
                    playersQuery.select('name', 'email')
                })
            })
            .where('venue_id', request.params().id)
            .where('id', request.params().field_id)
            .first()
            if(field != null)
            {
                return response.ok({ message: 'Success!', data: field})
            }
            else
            {
                return response.notFound({ message: 'Failed to show field data'})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed show field data', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed show field data', error: error.message})
            }
        }
    }

}

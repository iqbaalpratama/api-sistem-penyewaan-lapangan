import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserValidator from 'App/Validators/UserValidator'
import User from 'App/Models/User'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'
import { schema,rules } from '@ioc:Adonis/Core/Validator'

export default class AuthController {
/**
  * @swagger
  * /api/v1/register:
  *   post:
  *     tags:
  *         - Authentification
  *     summary: Registering New User
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          name:
  *                              type: string
  *                          email:
  *                              type: string
  *                          password:
  *                              type: string
  *                      required:
  *                          - name
  *                          - email
  *                          - password
  *     responses:
  *       201:
  *         description: Register success
  *       422:
  *         description: Register failed, invalid data request
  *       400:
  *         description: Register failed
  * /api/v1/login:
  *   post:
  *     tags:
  *         - Authentification
  *     summary: Login to get bearer token
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          email:
  *                              type: string
  *                          password:
  *                              type: string
  *                      required:
  *                          - email
  *                          - password
  *     responses:
  *       200:
  *         description: Login success
  *       422:
  *         description: Login failed, invalid data request
  *       400:
  *         description: Login failed, user not found
  *       404:
  *         description: Login failed, user not found
  * /api/v1/otp-confirmation:
  *   post:
  *     tags:
  *         - Authentification
  *     summary: Verify the account
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          email:
  *                              type: string
  *                          otp_code:
  *                              type: string
  *                      required:
  *                          - email
  *                          - otp_code
  *     responses:
  *       200:
  *         description: OTP Confirmation success
  *       404:
  *         description: OTP Confirmation failed, user not found
  *       400:
  *         description: OTP Confirmation, failed, validate data input
  */
    public async register({request, response} : HttpContextContract){
        try {
            const data = await request.validate(UserValidator)
            let role = 'user'
            if(request.input('role') != undefined)
            {
                role = 'owner'
            }
            const newUser = await User.create({
                name: data.name,
                email: data.email,
                password: data.password,
                role
            })
            const otp_code = Math.floor(100000 + Math.random() * 900000)
            await Database.table('otp_codes').insert({
                otp_code: otp_code,
                user_id: newUser.id
            })
            await Mail.send((message) => {
                message
                  .from('iqbaalpratama@sanberdev.com')
                  .to(data.email)
                  .subject('Registration success!')
                  .htmlView('email/otp_confirmation', { otp_code })
              })
            return response.created({ message: 'Register success, please verify your OTP Code in your email'})
        } catch (error) {
            if(error.messages != undefined)
            {
                return response.badRequest({ message: "Failed to register!", errors: error.messages })
            }
            else
            {
                return response.unprocessableEntity({ message: "Failed to register!", errors: error.message })
            }
        }
    }
    public async login({request, response, auth} : HttpContextContract){
        await request.validate({
            schema: schema.create({
                email: schema.string({}, [
                    rules.email(),
                ]),
                password: schema.string({}, [
                    rules.minLength(6),
                ]),
            })
        })
        const email = request.input('email')
        const password = request.input('password')
        try
        {
            const token = await auth.use('api').attempt(email, password)
            return response.ok({ message: 'Login success!', token: token.token})
        }
        catch(error)
        {
            if(error.messages != undefined)
            {
                return response.badRequest({ message: "Failed to login!", errors: error.messages })
            }
            else
            {
                return response.notFound({ message: "Failed to login!", errors: error.message })
            }
        }
    }

    public async otpChecking({request, response} : HttpContextContract)
    {
        try {
            await request.validate({
                schema: schema.create({
                    email: schema.string({}, [
                        rules.email(),
                    ]),
                    otp_code: schema.string({}, [
                        rules.minLength(6),
                    ]),
                })
            })
            let otp_code = request.input('otp_code')
            let email = request.input('email')
            let user = await User.findBy('email', email)
            let otp = await Database.query().from('otp_codes').where('otp_code', otp_code).first()
            if(user?.id == otp.user_id)
            {
                const verified_user = await User.findOrFail(user?.id)
                verified_user.is_verified = true
                await verified_user.save()
                return response.ok({message: 'OTP Confirmation success, you can login with your email and password now'})
            }
            else
            {
                return response.notFound({ message: 'OTP Confirmation failed, user not found'})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                return response.badRequest({ message: 'OTP Confirmation failed', error: error.messages})
            }
        }

    }
}

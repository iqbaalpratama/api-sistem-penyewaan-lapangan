// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator'
import Field from 'App/Models/Field'
import User from 'App/Models/User'
import Booking from 'App/Models/Booking'

export default class BookingsController {

    /**
  * @swagger
  * /api/v1/bookings:
  *   get:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Get all data bookings
  *     responses:
  *       200:
  *         description: success get all data bookings
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Not found
  *       400:
  *         description: Failed get all data bookings
  *   post:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Store data booking
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          field_id:
  *                              type: number
  *                          play_date_start:
  *                              type: string
  *                          play_date_end:
  *                              type: string
  *                      required:
  *                          - field_id
  *                          - play_date_start
  *                          - play_date_end
  *     responses:
  *       201:
  *         description: Success store data booking
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Field not found
  *       400:
  *         description: Failed store data venue, fails validate data request
  * /api/v1/bookings/{id}:
  *   get:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Get data booking by id
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: booking id to get
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: Success show data booking by id
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Booking not found
  *       400:
  *         description: Failed show data booking
  *   put:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Update data booking
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: booking id to update
  *        schema:
  *          type : integer
  *     requestBody:
  *         required: true
  *         content:
  *             application/x-www-form-urlencoded:
  *                  schema:
  *                      type: object
  *                      properties:
  *                          field_id:
  *                              type: number
  *                          play_date_start:
  *                              type: string
  *                          play_date_end:
  *                              type: string
  *                      required:
  *                          - field_id
  *                          - play_date_start
  *                          - play_date_end
  *     responses:
  *       200:
  *         description: Success update data booking
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Booking not found
  *       400:
  *         description: Failed update data booking, fails validate data request
  *   delete:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Delete data venue by id
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: booking id to delete
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: Success delete data booking
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Booking not found
  *       400:
  *         description: Failed delete data booking
  * /api/v1/bookings/{id}/join:
  *   put:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Join in an existing booking
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: booking id to join
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: Success join on a booking or have joined to that booking
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: Booking not found
  *       400:
  *         description: Failed join booking
  * /api/v1/bookings/{id}/unjoin:
  *   put:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Unjoin booking
  *     parameters:
  *      - name: id
  *        in: path
  *        required: true
  *        description: booking id to unjoin
  *        schema:
  *          type : integer
  *     responses:
  *       200:
  *         description: Success join booking
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: User haven't joined the booking or booking not found
  *       400:
  *         description: Failed unjoin booking
  * /api/v1/schedules:
  *   get:
  *     tags:
  *         - Bookings
  *     security:
  *         - bearerAuth: []
  *     summary: Get schedule of user
  *     responses:
  *       200:
  *         description: Success get schedule
  *       401:
  *         description: User Unauthorized
  *       404:
  *         description: User haven't join any booking
  *       400:
  *         description: Failed get schedule
  */
    public async index({response}: HttpContextContract) {
        try {
            const booking = await Booking
            .query()
            .preload('field', (fieldQuery) => {
                fieldQuery.select('name', 'type', 'venue_id')
                fieldQuery.preload('venue', (venueQuery) => {
                    venueQuery.select('name', 'address', 'phone')
                })
            })
            .preload('players', (playersQuery) => {
                playersQuery.select('name', 'email')
            })

            if( booking != null)
            {
                return response.ok({ message: 'Success!', data: booking})
            }
            else
            {
                return response.notFound({ message: 'Failed get booking data'})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed get booking data', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed get booking data', error: error.message})
            }
        }
    }

    public async store({request, response, auth}: HttpContextContract) {
        try {
            const data = await request.validate(BookingValidator)
            const field = await Field.findOrFail((parseInt(request.input('field_id'))))
            const user = await auth.user
            const booking = new Booking()
            booking.play_date_start = data.play_date_start
            booking.play_date_end = data.play_date_end
            await field.related('bookings').save(booking)
            await user?.related('bookings').save(booking)
            await user?.related('players').attach([booking.id])
            return response.created({ message: 'Success book the field'})
        } catch (error) {
            if(error.messages == undefined)
            {
                response.notFound({ message: 'Failed to book the field, field not found' , error: error.message})
            }
            else
            {
                response.badRequest({ message: 'Failed to book the field', error: error.messages})
            }
        }
    }
    public async join({request, response, auth}: HttpContextContract) {
        try {
            const booking = await Booking.findOrFail((parseInt(request.params().id)))
            const user = await auth.user
            const userLogin = await User.findOrFail(user?.id)
            const data = await Booking
            .query()
            .preload('players', (playersQuery) => {
                playersQuery.where('users.id', userLogin.id)
            })
            .where('bookings.id', parseInt(request.params().id))
            .first()
            if(data?.players.length == 0)
            {
                await user?.related('players').attach([booking.id])
                return response.ok({ message: 'Success join booking'})
            }
            else
            {
                return response.ok({ message: 'You have joined this booking before!'})
            }


        } catch (error) {
            if(error.messages == undefined)
            {
                response.notFound({message: 'Failed join booking, booking not found', error: error.message})
            }
            else
            {
                response.badRequest({message: 'Failed join booking', error: error.messages})
            }
        }
    }

    public async unjoin({request, response, auth}: HttpContextContract) {
        try {
            const booking = await Booking.findOrFail((parseInt(request.params().id)))
            const user = await auth.user
            const userLogin = await User.findOrFail(user?.id)
            const data = await Booking
            .query()
            .preload('players', (playersQuery) => {
                playersQuery.where('users.id', userLogin.id)
            })
            .where('bookings.id', parseInt(request.params().id))
            .first()
            if(data?.players.length != 0)
            {
                await user?.related('players').detach([booking.id])
                return response.ok({ message: 'Success unjoin booking'})
            }
            else
            {
                return response.notFound({ message: 'You are not joined in this booking before, join the booking before unjoin it'})
            }

        } catch (error) {
            if(error.messages == undefined)
            {
                response.notFound({message: 'Failed unjoin booking, booking not found', error: error.message})
            }
            else
            {
                response.badRequest({message: 'Failed unjoin booking', error: error.messages})
            }
        }
    }
    public async show({request, response}: HttpContextContract) {
        try {
            const booking = await Booking
            .query()
            .preload('field', (fieldQuery) => {
                fieldQuery.select('name', 'type', 'venue_id')
                fieldQuery.preload('venue', (venueQuery) => {
                    venueQuery.select('name', 'address', 'phone')
                })
            })
            .preload('players', (playersQuery) => {
                playersQuery.select('name', 'email')
            })
            .where('bookings.id', request.params().id)
            .first()
            if( booking != null)
            {
                return response.ok({ message: 'Success!', data: booking})
            }
            else
            {
                return response.notFound({ message: 'Failed get booking data, booking not found'})
            }

        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed get booking data', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed get booking data', error: error.message})
            }
        }
    }

    public async update({request, response, auth}: HttpContextContract) {
        try {
            await request.validate(BookingValidator)
            const user = await auth.user
            const booking = await Booking.findOrFail(request.params().id)
            const data = await Booking
            .query()
            .preload('user')
            .where('bookings.id', parseInt(request.params().id))
            .first()
            if(data?.user.id !=  user?.id)
            {
                return response.badRequest({ message: 'You dont have right to update this booking'})
            }
            else
            {
                const field = await Field.findOrFail(parseInt(request.input('field_id')))
                booking.play_date_start = request.input('play_date_start'),
                booking.play_date_end = request.input('play_date_end')
                await booking.related('field').dissociate()
                await booking.related('field').associate(field)
                await booking.save()
                return response.ok({ message: 'Success update booking!'})
            }
        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed update booking', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed update booking', error: error.message})
            }

        }
    }

    public async destroy({request, response, auth}: HttpContextContract) {
        try {
            const user = await auth.user
            const booking = await Booking.findOrFail(request.params().id)
            const data = await Booking
            .query()
            .preload('user')
            .where('bookings.id', parseInt(request.params().id))
            .first()
            if(data?.user.id !=  user?.id)
            {
                return response.unauthorized({ message: 'You dont have right to delete this booking'})
            }
            else
            {
                if(booking == null)
                {
                    return response.notFound({ message: 'Booking not found'})
                }
                await booking.delete()
                return response.ok({ message: 'Success delete booking!'})
            }
        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed delete booking', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed delete booking', error: error.message})
            }

        }
    }

    public async getSchedules({response, auth}: HttpContextContract) {
        try {
            const user = await auth.user
            const userLogin = await User.findOrFail(user?.id)
            const data = await User
            .query()
            .select('users.id', 'users.name', 'users.email')
            .preload('players', (playersQuery)=>{
                playersQuery.as('schedules')
                playersQuery.select('bookings.play_date_start', 'bookings.play_date_end', 'bookings.field_id')
                playersQuery.preload('field', (fieldQuery) => {
                    fieldQuery.select('fields.name', 'fields.type', 'fields.venue_id')
                    fieldQuery.preload('venue', (venueQuery) => {
                        venueQuery.select('venues.name', 'venues.address', 'venues.phone')
                    })
                })
            })
            .where('users.id', userLogin.id)
            .first()
            if(data?.players.length == 0)
            {
                return response.notFound({ message: 'You dont have schedule, please join the booking before'})
            }
            else
            {
                return response.ok({ message: 'Success get schedule!', data})
            }
        } catch (error) {
            if(error.messages != undefined)
            {
                response.badRequest({message: 'Failed get schedule', error: error.messages})
            }
            else
            {
                response.notFound({message: 'Failed get schedule', error: error.message})
            }

        }
    }
}

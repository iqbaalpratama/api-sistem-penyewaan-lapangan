/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'


Route.group(() => {
  Route.group(() => {
    Route.post('/login', 'AuthController.login').as('login')
    Route.post('/register', 'AuthController.register').as('register')
    Route.post('/otp-confirmation', 'AuthController.otpChecking').as('otp_check')
    Route.group(() => {
      Route.get('/venues', 'VenuesController.index' ).as('venues.index')
      Route.post('/venues', 'VenuesController.store' ).as('venues.store')
      Route.get('/venues/:id', 'VenuesController.show').as('venues.show')
      Route.put('/venues/:id', 'VenuesController.update').as('venues.update')
      Route.get('/venues/:id/fields', 'FieldsController.index').as('fields.index')
      Route.post('/venues/:id/fields', 'FieldsController.store').as('fields.store')
      Route.get('/venues/:id/fields/:field_id', 'FieldsController.show').as('fields.show')
    }).middleware(['auth','verify', 'role:owner'])
    Route.group(() => {
      Route.post('/bookings', 'BookingsController.store' ).as('bookings.store')
      Route.get('/bookings', 'BookingsController.index' ).as('bookings.index')
      Route.get('/bookings/:id', 'BookingsController.show' ).as('bookings.show')
      Route.get('/schedules', 'BookingsController.getSchedules').as('bookings.schedule')
      Route.put('/bookings/:id/join', 'BookingsController.join').as('bookings.join')
      Route.put('/bookings/:id/unjoin', 'BookingsController.unjoin').as('bookings.unjoin')
      Route.put('/bookings/:id', 'BookingsController.update' ).as('bookings.update')
      Route.delete('/bookings/:id', 'BookingsController.destroy' ).as('bookings.delete')
    }).middleware(['auth','verify', 'role:user'])
  }).prefix('/v1')
}).prefix('/api')

import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UsersSchema extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name', 45).notNullable()
      table.string('email', 45).unique().notNullable()
      table.string('password').notNullable()
      table.string('role', 45).notNullable()
      table.boolean('is_verified').defaultTo('false')

      /**
       * Uses timestampz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
